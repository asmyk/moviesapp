import { FireDOM } from "./FireDOM.js";

/**
 * Base class for every component.
 */
class BaseComponent {
    constructor(props) {
        this.props = props || {};
        this.update = this.update.bind(this);
        this.render = this.render.bind(this);
        this.state = {};
        this._dom = null;
    }

    getRenderedElement() {
        this._dom = this.render(this.props);
        return this._dom;
    }

    /**
     * Updates current component's childrens
     */
    update() {
        let newDom;
        if (this._dom) {
            newDom = this.render(this.props);
            FireDOM.updateElement(this._dom, newDom, this._dom.parentNode);
            this._dom = newDom;
        } else {
            throw new Error(`Cannot update component ${this.toString()}. The component DOM element is not rendered.`)
        }
    }

    setState(newState) {
        let oldState = this.state;

        this.state = Object.assign({}, oldState, newState);
        return this.state;
    }

    /**
     * Overdriven by components. Return rendered DOM node
     */
    render() {
        return FireDOM.createElement("div");
    }
}

export { BaseComponent };