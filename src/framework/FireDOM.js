
/**
 * FireDOM is a simple React-like framework.
 * It is based on recursive elements creation. The concept is simple:
 * 1. Attach root component to dom node
 * 2. Render all root's children and attach them into root DOM NODE
 * 3. Traverse all root's childrens and back to point 2
 * 4. Application tree is rendered in browser.
 * 
 * This is simple for an interview process. It let's user to prototype code very fast and easy.
 */
function FireDOM() { }

/**
 * A main framework function. It's connect our app represented by root node with document element
 * @param {*} app - main app's node
 * @param {*} element - document element for framework place
 */

FireDOM.prototype.render = function (app, element) {
    return this.mountElement(app, element);
}

/**
 * Create new view node
 * @param {*} type - DOM type - "div", "span", "h1" etc
 * @param {*} props - properties that should be added to view
 * @param {*} childrens - children nodes
 */

FireDOM.prototype.createElement = function (type = "div", props = {}, ...childrens) {
    var newNode;
    if (typeof type === "function") {
        newNode = this.createComponent(type, props)
    } else {
        newNode = this.createNode(type, props);
        if (Array.isArray(childrens) && childrens.length > 0) {

            childrens.forEach((childItem) => (
                this.mountElement(childItem, newNode)
            ));
        }
        else if (typeof childrens === "string") {
            this.mountElement(childrens, newNode);
        }
    }
    return newNode;
}

/**
 * Put component data into DOM and render as a DOM tree
 */
FireDOM.prototype.createComponent = function (component, props) {
    var domElement, instance;

    // simply check if component is class of function
    if (component.prototype.render && typeof component.prototype.render === "function") {
        instance = new component(props);
        domElement = instance.getRenderedElement(props);
    } else {
        domElement = component(props);
    }

    return domElement;
}

/**
 * Update given element(and childrens) in parent node
 */
FireDOM.prototype.updateElement = function (oldNode, newNode, parent, index = 0) {
    this.replaceElement(newNode, oldNode, parent);
}


/**
 * Append given element to parent element
 */
FireDOM.prototype.mountElement = function (element, parentElement) {
    return parentElement.appendChild(element);
}

/**
 * Removes given node from parent element
 */
FireDOM.prototype.unmountElement = function (element, parentElement) {
    return parentElement.removeChild(element);
}

/**
 * Replace oldElement with newElement on given parent
 */
FireDOM.prototype.replaceElement = function (newElement, oldElement, parentNode) {
    return parentNode.replaceChild(newElement, oldElement);
}

/**
 * Creates new dom element
 * @param {*} type - node type
 * @param {*} props - props added to element
 */
FireDOM.prototype.createNode = function (type, props) {
    var newNode = document.createElement(type),
        keys = Object.keys(props)

    for (let prop of keys) {
        // set class name
        if (prop === "className" || prop === "class") {
            newNode.classList.add(props[prop]);
        }
        // inline style
        if (prop === "style") {
            newNode.style.cssText += props[prop];
        }
        // set inner html
        if (prop === "text") {
            newNode.innerText = props[prop];
        }
        // bind events
        if (prop.startsWith("on")) {
            newNode[prop.toLocaleLowerCase()] = props[prop];
        }
        // setup html node attributes
        if (prop === "attrs") {
            for (let attribute in props[prop]) {
                newNode.setAttribute(attribute, props[prop][attribute]);
            }
        }
    }

    return newNode;
};


// return singleton instance
const FireDOMInstance = new FireDOM();
export { FireDOMInstance as FireDOM };