import mainStyle from "./main.css";
import { FireDOM } from "./framework/FireDOM.js";
import { AppComponent } from "./components/App/App.js";

import "normalize.css";


document.addEventListener('DOMContentLoaded', function () {
    var rootEl = document.getElementById("rootElement");

    FireDOM.render(FireDOM.createElement("div", {}, FireDOM.createElement(AppComponent, {})), rootEl);
});