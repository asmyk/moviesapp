class ReqService {
    async getRequest(url) {
        try {
            const response = await fetch(url);
            return response.json();
        } catch (err) {
            return { status: ERROR, data: err };
        }
    }
}

// singleton base service
const Service = new ReqService();

export { Service };