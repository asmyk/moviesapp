import { SEARCH_URL } from "api.js";
import { Service } from "./Service.js";

class MoviesDBService {
    getMovies(searchString, page = 1) {
        let url = SEARCH_URL.concat(searchString).concat("&page=" + page);
        return Service.getRequest(url);
    }
}

const MoviesService = new MoviesDBService();

export { MoviesService };