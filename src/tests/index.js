var assert = require('assert');
import { JSDOM } from "jsdom"
import { FireDOM } from "../framework/FireDOM.js";
import { BaseComponent } from "../framework/FireDOM.component.js";

const dom = new JSDOM('<body><div id="root"></div></body>');
const document = dom.window.document;
global.document = document;

describe('FireDOM', function () {
    it('should mount root component to DOM node', function () {
        let root = document.getElementById("root"),
            appElement = document.createElement("div");

        FireDOM.render(appElement, root);
        assert.ok(root.children[0].isEqualNode(appElement));
    });

    it("should create div node", function () {
        let node = FireDOM.createElement("div", { className: "myclass" });
        assert.equal(node.tagName, "DIV");
        assert.equal(node.classList[0], "myclass");
    });

    it("should create div node's children", function () {
        let node = FireDOM.createElement("div", { className: "myclass" }, FireDOM.createElement("span"));
        assert.equal(node.children[0].tagName, "SPAN");
    });

    it("should create element from stateless function", function () {
        const component = (props) => (FireDOM.createElement("div", props));
        let element = FireDOM.createElement(component, { className: "myclass" });

        assert.ok(element.tagName, "DIV");
        assert.equal(element.classList[0], "myclass");
    });


    it("should create element from class component", function () {
        class component extends BaseComponent { render(props) { return FireDOM.createElement("div", props) } };
        let element = FireDOM.createElement(component, { className: "myclass" });

        assert.ok(element.tagName, "DIV");
        assert.equal(element.classList[0], "myclass");
    });

    it("should create node with many childrens", function () {
        let element = FireDOM.createElement("span", {}, FireDOM.createElement("div", {}), FireDOM.createElement("div", {}), FireDOM.createElement("div", {}));
        assert.equal(element.children.length, 3);
    })
});

describe("FireDOM component", function () {
    it("should return rendered div element by default", function () {
        let component = new BaseComponent();
        assert.equal(component.getRenderedElement().tagName, "DIV");
    });

    it("should set correct state", function () {
        let component = new BaseComponent();
        // new component have empty state
        assert.equal(Object.keys(component.state).length, 0);
        // set some state
        component.setState({ a: "b" });
        assert.equal(Object.keys(component.state).length, 1);
        // mix with current state
        component.setState({ b: "b" });
        assert.equal(Object.keys(component.state).length, 2);
        assert.equal(component.state["a"], "b");
        // update current state properties
        component.setState({ a: "w", b: "w" });
        assert.equal(Object.keys(component.state).length, 2);
        assert.equal(component.state["b"], "w");
        assert.equal(component.state["a"], "w");
    });
});