
import { FireDOM } from "framework/FireDOM.js";
import { MoviesService } from "services/MoviesService.js";

import { SearchComponent } from "components/search/Search.component.js";
import { TileListComponent } from "components/tileList/TileList.component.js";
import { BaseComponent } from "framework/FireDOM.component.js";
import { MessageComponent } from "components/message/Message.component.js";

import style from "main.css";

class AppComponent extends BaseComponent {
    constructor(props) {
        super(props);
        
        this.handleTextSearch = this.handleTextSearch.bind(this);
        this.handleLoadNextPage = this.handleLoadNextPage.bind(this);
        this.handleLoadPrevPage = this.handleLoadPrevPage.bind(this);

        this.state = { movies: "", initialRender: true, lastSearchValue: "" };
    }

    /**
     * Handle search input changes and call service only when search is necessary
     * @param {*} searchValue - string
     */
    handleTextSearch(searchValue) {
        // handle empty input
        if (!searchValue) {
            this.setState({ "movies": "", initialRender: true, lastSearchValue: "" });
            return this.update();
        }

        // break when user click enter too many times
        if (searchValue && searchValue === this.state.lastSearchValue) {
            return;
        }

        // set state on LOADING and show message
        this.setState({ "movies": { results: "", status: "LOADING" }, page: 1, initialRender: false, lastSearchValue: searchValue });
        this.update();
        this.updateMovies();
    }

    handleLoadNextPage() {
        this.setState({ page: this.state.page + 1 });
        this.updateMovies();
    }

    handleLoadPrevPage() {
        this.setState({ page: this.state.page - 1 });
        this.updateMovies();
    }

    /**
     * Call a movies service for new movies search
     */
    updateMovies() {
        MoviesService.getMovies(this.state.lastSearchValue, this.state.page || 1).then(data => {
            if (data.results && data.results.length > 0) {
                this.setState({ "movies": { results: data, status: "OK" } })
            } else {
                this.setState({ "movies": { results: "", status: "NOT_FOUND" } });
            }
            this.update();
        });
    }

    render() {
        let search = FireDOM.createElement(SearchComponent, { className: style.searchSection, handleInputChange: this.handleTextSearch, value: this.state.lastSearchValue });
        let tiles = FireDOM.createElement(TileListComponent, { data: this.state.movies, onPrevClick: this.handleLoadPrevPage, onNextClick: this.handleLoadNextPage });

        if (this.state.initialRender) {
            tiles = FireDOM.createElement(MessageComponent, { text: "What kind of movie do you like? Start typing..." })
        }

        return FireDOM.createElement("div", { className: style.wrapper }, search, tiles);
    };
}

export { AppComponent };