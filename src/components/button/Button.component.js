
import { FireDOM } from "framework/FireDOM.js";
import style from "main.css";

const ButtonComponent = (props) => (
    FireDOM.createElement("button", Object.assign(props, { className: style.btn }))
)

export { ButtonComponent };