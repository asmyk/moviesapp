import { FireDOM } from "framework/FireDOM.js";
import { InputComponent } from "components/input/Input.component.js";
import { LogoComponent } from "components/logo/Logo.component.js";

import { ButtonComponent } from "components/button/Button.component.js";
import debounce from "throttle-debounce/debounce";

const SearchComponent = (props) => {
    let input = FireDOM.createElement(InputComponent, { handleInputChange: debounce(500, props.handleInputChange), value: props.value });
    let logo = FireDOM.createElement(LogoComponent);
    let button = FireDOM.createElement(ButtonComponent, { text: "Search", style: "grid-column: 3;height:72px", onClick: () => (debounce(500, props.handleInputChange(input.value))) });

    return FireDOM.createElement("div", props, logo, input, button);
}


export { SearchComponent };