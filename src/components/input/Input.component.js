import { BaseComponent } from "framework/FireDOM.component.js";
import { FireDOM } from "framework/FireDOM.js";
import style from "main.css";

class InputComponent extends BaseComponent {
    constructor(props) {
        super(props);
        this.onInputChange = this.onInputChange.bind(this);
    }
    onInputChange(e) {
        if (e.keyCode === 13) {
            this.props.handleInputChange(e.target.value, e);
        }
    }

    render(props) {
        const attrs = {
            autofocus: true,
            placeholder: "Search", value: props.value || "",
        };
        return FireDOM.createElement("input", { attrs, className: style.searchInput, onKeyUp: this.onInputChange });
    }
}

export { InputComponent };