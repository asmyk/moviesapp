import { FireDOM } from "framework/FireDOM.js";
import { ButtonComponent } from "components/button/Button.component.js";
import style from "main.css";

const PaginationComponent = (props) => {
    let { page, total_pages, onPrevClick, onNextClick } = props;

    const prevBtn = FireDOM.createElement(ButtonComponent, { text: "Previous", attrs: { [page === 1 ? "disabled" : "enabled"]: "" }, onClick: onPrevClick });
    const pageText = FireDOM.createElement("span", { text: `Page ${page} of ${total_pages}` });
    const nextBtn = FireDOM.createElement(ButtonComponent, { text: "Next", attrs: { [page === total_pages ? "disabled" : "enabled"]: "" }, onClick: onNextClick });


    return FireDOM.createElement("div", { className: style.pagination }, prevBtn, pageText, nextBtn);
}


export { PaginationComponent };
