import { FireDOM } from "framework/FireDOM.js";
import { IMG_URL } from "api.js";
import style from "main.css";
import backupImg from "assets/images/backup-image.png";

const TileComponent = (props) => {
    const img = FireDOM.createElement("img", { className: style.tileImg, attrs: { src: IMG_URL + props.poster_path, onerror: `this.src='${backupImg}'` } });
    return FireDOM.createElement("li", { className: style.tile }, img);
}

export { TileComponent };