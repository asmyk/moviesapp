import { BaseComponent } from "framework/FireDOM.component.js";
import { FireDOM } from "framework/FireDOM.js";
import { TileComponent } from "components/tile/Tile.component.js";
import { PaginationComponent } from "components/pagination/Pagination.component.js";
import { MessageComponent } from "components/message/Message.component.js";
import style from "main.css";

class TileListComponent extends BaseComponent {
    constructor(props) {
        super(props);
        this.getTile = this.getTile.bind(this);
    }

    getTile(data) {
        return FireDOM.createElement(TileComponent, data);
    }

    render(props) {
        let { data, data: { results: { results } = "" } } = props;

        if (data.status === "LOADING") {
            return this.renderLoading();
        } else if (data.status === "OK" && results.length > 0) {
            return FireDOM.createElement("div", {}, FireDOM.createElement(PaginationComponent, Object.assign(data.results, props)), FireDOM.createElement("ul", { className: style.tilesSection }, ...   this.renderResults(results)));
        }

        return this.renderNotFound();
    }

    renderResults(results) {
        return results.map((item) => (this.getTile(item)));
    }

    renderNotFound() {
        return FireDOM.createElement(MessageComponent, { text: "We don't found any movies matching to your search :(" });
    }

    renderLoading() {
        return FireDOM.createElement(MessageComponent, { text: "Loading..." });
    }
}

export { TileListComponent };