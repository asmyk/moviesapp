import { FireDOM } from "framework/FireDOM.js";
import style from "main.css";

const LogoComponent = () => (FireDOM.createElement("span", { className: style.logo, text: "M" }))
export { LogoComponent };