import { FireDOM } from "framework/FireDOM.js";
import style from "main.css";

const MessageComponent = (props) => {
    return FireDOM.createElement("div", { className: style.message, text: props.text });
}

export { MessageComponent };