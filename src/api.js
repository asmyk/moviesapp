const API_KEY = "your_key", // enter your api key here
    API_URL = "https://api.themoviedb.org/3",
    IMG_URL = "https://image.tmdb.org/t/p/w300",
    SEARCH_URL = `${API_URL}/search/movie?api_key=${API_KEY}&query=`;


export { SEARCH_URL,IMG_URL };