# MoviesDB APP

This app is coded for an interview. It's based on simple React-based framework called FireDOM which was created for this app.

## Code Sample

For this task I implemented simple FireDOM framework. It build DOM tree from components and childrens recursively. Example:

Mount top-level component:

```
  let root = document.getElementById("root");
  let appElement = FireDOM.createElement(App);

  // mount top-level component to body
  FireDOM.render(appElement, root);
```

Render childrens:
```
const App = (props) => {
    return FireDOM.createElement("div", props, HeaderComponent, ContentComponent, FooterComponent);
}
```

This code render our App component(and his childrens) in given root node.


## Getting Started

You should have installed node(>8.0.0) and npm.


### Installing

First, you should install dependecies:

```
npm install
```

### Development mode

To test app on dev-server run

```
npm run dev
```

The server will be available at http://localhost:8080

## Running the tests

To run unit tests enter:

```
npm run test
```


## Deployment

To create production package run

```
npm run build
```

And package will be ready to upload in /dist folder